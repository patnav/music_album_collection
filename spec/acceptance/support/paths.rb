module NavigationHelpers
  # Put helper methods related to the paths in your application here.

  def homepage
    "/"
  end

  def new_user_session
  	"/users/sign_in"
  end

  def albums_index
  	"/albums"
  end

  def new_album
  	"/albums/new"
  end

end

RSpec.configuration.include NavigationHelpers, :type => :acceptance