require 'acceptance/acceptance_helper'

feature 'Albums', %q{
  In order to have a nice music album collection
  As a user
  I want to create and manage albums
} do

  background do
  	@user = User.create!(:email => 'user@example.com', :password => 'password')
  	Album.create!(:title => 'One of the Boys', :artist => 'Katy Pery', :description => '', :user_id => @user.id)
  	Album.create!(:title => 'Fearless', :artist => 'Taylor Swift', :description => '', :user_id => @user.id)
  end

  scenario 'Viewing album list' do
  	visit new_user_session
	
	fill_in 'Email', :with => 'user@example.com'
	fill_in 'Password', :with => 'password'
  	
  	click_button 'Sign in'

  	visit albums_index

  	page.should have_content('One of the Boys')
  	page.should have_content('Fearless')
  end

  scenario 'Creating an album' do
  	visit new_user_session
	
	fill_in 'Email', :with => 'user@example.com'
	fill_in 'Password', :with => 'password'
  	
  	click_button 'Sign in'

  	visit new_album
  	Album.create!(:title => 'Ocean Eyes', :artist => 'Owl City', :description => '', :user_id => @user.id)

  	visit albums_index
  	page.should have_content('Ocean Eyes')
  end

  scenario 'Editing an album' do
  	visit new_user_session
	
	fill_in 'Email', :with => 'user@example.com'
	fill_in 'Password', :with => 'password'
  	
  	click_button 'Sign in'

  	visit albums_index

  	Album.create!(:title => 'Ocean Eyes', :artist => 'Owl City', :description => '', :user_id => @user.id)
  	@album = Album.where(:title => 'Ocean Eyes').first
  	@album.update_attributes(:description => 'This is a description')

  	visit albums_index
  	page.should have_content('Ocean Eyes')
  end

  scenario 'Deleting an album' do
  	visit new_user_session
	
	fill_in 'Email', :with => 'user@example.com'
	fill_in 'Password', :with => 'password'
  	
  	click_button 'Sign in'

  	visit albums_index

  	Album.create!(:title => 'Ocean Eyes', :artist => 'Owl City', :description => '', :user_id => @user.id)
  	@album = Album.where(:title => 'Ocean Eyes').first
  	@album.destroy

  	visit albums_index
  end


end
