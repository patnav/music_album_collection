require 'acceptance/acceptance_helper'

feature 'Users', %q{
  In order to use the music album collection
  As a user
  I want to sign up, sign in, and manage my account
} do

  background do
    User.create!(:email => 'user@example.com', :password => 'caplin')
  end

  scenario 'Signing in with correct credentials' do
  	visit new_user_session
	
	fill_in 'Email', :with => 'user@example.com'
	fill_in 'Password', :with => 'caplin'
  	
  	click_button 'Sign in'
  	visit albums_index
  end

  scenario 'Signing in with incorrect credentials' do
  	visit new_user_session
	
	fill_in 'Email', :with => 'user@example.com'
	fill_in 'Password', :with => 'password'
  	
  	click_button 'Sign in'
  	visit new_user_session
  end

end
