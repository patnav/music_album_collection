class Album < ActiveRecord::Base
  attr_accessible :title, :artist, :description, :cover, :user_id

  # Validations
  validates_presence_of :title, :artist, :user_id
  validates_uniqueness_of :title


  # Relationships
  belongs_to :user
  has_attached_file :cover, :styles => { :large => "640x640>", :medium => "300x300>", :thumb => "100x100>" }
end
