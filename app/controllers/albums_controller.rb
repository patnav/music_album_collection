class AlbumsController < ApplicationController
  def index
  	@albums = current_user.albums
  end

  def show
    @album = Album.find(params[:id])
  end

  def new
  	@album = Album.new
  end

  def edit
    @album = Album.find(params[:id])
  end

  def create
  	@album = Album.new(params[:album])
    @album.user_id = current_user.id
    
    if @album.save
      flash[:success] = "Album has been created"
      redirect_to :action => :index
    else
      render :action => :new
    end
  end

  def update
    @album = Album.find(params[:id])

    if @album.update_attributes(params[:album])
      flash[:success] = "Album was successfully updated"
      redirect_to :action => :index
    else
      render :action => :edit
    end
  end
  
  def destroy
    @album = Album.find(params[:id])
    @album.destroy
    
    flash[:success] = "Album was successfully deleted"
    redirect_to :action => :index
  end
end
