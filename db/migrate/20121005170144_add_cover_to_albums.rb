class AddCoverToAlbums < ActiveRecord::Migration
  def change
  	add_attachment :albums, :cover
  end
end
